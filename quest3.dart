class App {  
   // field 
   var name;
   var category;
   var developer;
   var year;

   showAppInfo() {  
        print("App Name: "+name.toUpperCase());  
        print("Category: $category");  
        print("Developer: $developer");
        print("Year: $year");
      }  
}

void main() {
   var app = new App();
   app.name = "Ambani Africa";
   app.category = "Best Gaming Solution";
   app.developer = "Mukundi Lambani";
   app.year = 2021;
   app.showAppInfo();
}